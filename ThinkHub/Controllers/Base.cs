﻿namespace ThinkHub.Controllers
{
	public class Session
	{
		public const string SessionID = "SessionID";
		public const string UserName = "UserName";
		public const string UserImage = "UserImage";
		public const string CurrentPath = "CurrentPath";
		public class Clipboard
		{
			public const string Type = "Clipboard-Type";
			public const string FilePath = "Clipboard-File-Path";
			public const string FileType = "Clipboard-File-Type";
		}
	}
}